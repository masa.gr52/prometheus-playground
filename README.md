# Overview
Collect Kubernetes metrics by Prometheus

## 0. Preparation
* create new project
* Create New Cluster
```
$ gcloud container clusters create <CLUSTER-NAME>
```
* Get credentials
```
$ gcloud container clusters get-credentials <CLUSTER-NAME> --zone <ZONE> --project <PROJECT-NAME>
```

## 1. Create Namespace
* Create new namespace for monitoring by Prometheus
```
$ kubectl apply -f https://gitlab.com/masa.gr52/prometheus-playground/raw/master/namespace.yaml
```

## 2. Configure RBAC
* Create ClusterRoleBinding(To grant permissions to their own serviceaccount)
```
$ gcloud info | grep Account
$ kubectl create clusterrolebinding cluster-admin-binding \
  --clusterrole=cluster-admin \
  --user=<ACCOUNT>
```

* Create ServiceAccount/ClusterRole/Binding for prometheus
```
$ kubectl apply -f https://gitlab.com/masa.gr52/prometheus-playground/raw/master/rbac.yaml
```
## 3. Create ConfigMap
```
$ kubectl apply -f https://gitlab.com/masa.gr52/prometheus-playground/raw/master/prometheus-config.yaml
```

## 4. Deployment 
* Create Deployment
```
$ kubectl apply -f https://gitlab.com/masa.gr52/prometheus-playground/raw/master/deployment-prometheus.yaml
```

* Create Service
```
$ kubectl apply -f https://gitlab.com/masa.gr52/prometheus-playground/raw/master/service-prometheus.yaml
```

## 5. Add node exporter 
```
$ kubectl apply -f https://gitlab.com/masa.gr52/prometheus-playground/raw/master/node-exporter-ds.yaml
```

-----

## note. construct high availability
* Install Thanos

note:
 install Helm by gofish (ref:https://gofi.sh/index.html#install)
```
$ curl -fsSL https://raw.githubusercontent.com/fishworks/gofish/master/scripts/install.sh | bash
$ gofish init
$ gofish upgrade gofish
$ gofish install helm
```

put values.yaml at local

```
$ kubectl --namespace kube-system create serviceaccount tiller

$ kubectl create clusterrolebinding tiller-cluster-rule \
 --clusterrole=cluster-admin --serviceaccount=kube-system:tiller

$ kubectl --namespace kube-system patch deploy tiller-deploy \
 -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}' 

$ helm init

$ helm list

$ helm repo update

$ helm delete --purge prometheus-thanos-sidecar

$ helm upgrade --version="8.6.0" --install --namespace=monitoring --values values.yaml prometheus-thanos-sidecar stable/prometheus
```

